import geometry.polygon as polygon
import geometry.primitives as primitives
import pygame
from pygame.locals import *


WHITE = pygame.Color(255, 255, 255)
RED = pygame.Color(255,0,0)
LIGHT_BLUE = pygame.Color(200, 200, 255)

_shape_drawing_dict = dict()

def draw_point_large(surface_dest:pygame.Surface, point : primitives.AbstractPoint, color = WHITE):
        """this will draw a disk of 5 pixel radius with gradual transparency"""
        coordonate = (point.x -5, point.y -5) # coordonate of the surface on the screen

        surface_temp = pygame.Surface((10, 10), flags=SRCALPHA)

        rawcolor = color[:3]
        transparency = color[3]

        pygame.draw.circle(surface_temp, rawcolor + (transparency*0.3,), (5, 5), 5)
        pygame.draw.circle(surface_temp, rawcolor + (transparency*0.6,), (5, 5), 3)
        pygame.draw.circle(surface_temp, color, (5, 5), 1)

        surface_dest.blit(surface_temp, coordonate)

def draw_segment(surface_dest:pygame.Surface, segment : primitives.AbstractSegment , color = WHITE):
    draw_with_points(surface_dest, segment, color)

def draw_polygon(surface_dest:pygame.Surface, poly : polygon.Polygon , color = WHITE):
    draw_with_points(surface_dest, poly, color)


# somewhat generic fonction for anything that is a list of points linked by lines
def draw_with_points(surface: pygame.Surface, shape:primitives.Geometrics, color:pygame.Color, width = 1):
    """ this method assume there is a self.points iterable
    Draw straight lines between the points. close the line if self.closed"""
    try:
        closed = shape.closed
    except AttributeError:
        closed = False

    pygame.draw.lines(surface, color, closed, tuple(shape.points), width=width) # Warning shape.point do not always exists


def get_drawing_function(shape_type:type): # TODO: le dict par defaut en parametre
    for each_type in shape_type.mro():
        try:
            return _shape_drawing_dict[each_type]
        except KeyError:
            pass
    # if we reach this point, it means we fail finding a drawing function
    raise KeyError("cannot find a drawing function for type: " + shape_type.__name__)

def draw_shape(surface_dest:pygame.Surface, shape : primitives.Geometrics , color = WHITE): # TODO: add width=1
    funct = get_drawing_function(type(shape))
    funct(surface_dest, shape, color)

_shape_drawing_dict[primitives.AbstractPoint]= draw_point_large
_shape_drawing_dict[primitives.AbstractSegment]= draw_with_points
_shape_drawing_dict[polygon.Polygon]= draw_with_points

