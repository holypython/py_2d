from abc import ABC, abstractmethod # ABC stand for AbstractBaseClass

class Drawable(ABC): # reminder: ABC stand for AbstractBaseClass
    """ a Drawable Object either is a point (inherit abstract point) or as a `points`property
     giving an iterable of points.
     An isolated point will be displayed separatly while a list of point will be linked by segments"""
    def __init__(self, color):
        self.color = color

    @abstractmethod
    def draw(self, surface):
        raise NotImplementedError
