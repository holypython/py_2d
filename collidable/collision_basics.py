from geometry.primitives import Segment, Point, Vector, dot_product, AbstractPoint

def intersection_segment_X_straight(segment:Segment, straight:"tuple(Point, Point)" ):
    """ collide if CD⃗.AD⃗ * CD⃗.BD⃗ ≤ 0
        dot products have opposite sign <=> angles (ADC) and (BDC) have opposite sign
         <=> A and B are on 2 different side of (CD)
         if 1 of the dot product is null this mean one of the points is right on the straight => collision
    """
    A,B = segment
    C,D = straight
    CD⃗ = Vector(C,D)
    AD⃗ = Vector(A,D)
    BD⃗ = Vector(B,D)

    CP = dot_product(CD⃗, AD⃗) * dot_product(CD⃗, BD⃗)
    # CP = (CD⃗.x*AD⃗.y - CD⃗.y*AD⃗.x) * (CD⃗.x*BD⃗.y - CD⃗.y*BD⃗.x) formule valide, mais pas d'optimisation anticipée
    return CP <= 0


def intersection_segment_X_segment_return_boolean(seg1, seg2):
    """ return True if segments intersect """

    """ notation: [AB] is a segment; (AB) is a straight and AB⃗ is a vector
    the logic is that:
    [AB] interesect [CD]
    <=>  [AB] intersect (CD) and (AB) intersect [CD]
    <=> CD⃗.AD⃗ * CD⃗.BD⃗  ≤ 0 and AB⃗.DB⃗ * AB⃗.CB⃗ ≤ 0
    """
    # TODO faire un test de collision de box
    return intersection_segment_X_straight(seg1, seg2) and intersection_segment_X_straight(seg2, seg1)


def intersection_segment_X_segment_return_shape(seg1:Segment , seg2:Segment):
    # Based on https://www.gamedev.net/forums/topic/503527-line-segment-colision/
        def det (AB,CD):
            return AB.x*CD.y-CD.x*AB.y

        # TODO Ca fonctionne, mais j'ai l'inpression que tester l'intersection des droites liées est bien mieux
        """ Soient les points A,B,C,D tq S1=(A,B) et S2=(C,D)"""
        A,B = seg1
        C,D = seg2

        AB = Vector(A,B)
        CD = Vector(C,D)
        AC = Vector(A,C)

        # det = AB.x*CD.y-CD.x*AB.y
        det_AB_CD = det(AB, CD)
        if det_AB_CD == 0:  # AB and CD parallel
            if det(AB, AC) == 0: # i.e. A,B,C,D are in line
                #assert isinstance((A,B,C,D), AbstractPoint)
                ordered_points = [A,B,C,D].sort(key = lambda point:point.x )
                return Segment(ordered_points[1],ordered_points[2]) # return the second and third points of the sorted list of 4. TODO: Bug if vertical line
            return None  # if parralele with no special case, intersect = None

        # at this point, we know segments are not parallele
        t = (AC.x * AB.y - AB.x * AC.y) / det_AB_CD
        s = (AC.x * CD.y - CD.x * AC.y) / det_AB_CD

        # assert(t == det(AC, AB)/det_AB_CD)
        # assert(s == det(AC, CD)/det_AB_CD)

        if t < 0 or t > 1 or s < 0 or s > 1:
            return None  # outside line segment

        if not isinstance(C,AbstractPoint):
            C = Point(C)
        Q = C + CD * t
        return Point(Q.x, Q.y)