import time

from geometry.primitives import *
from geometry.aabb_box import *
from geometry.polygon import *
from collidable import collision_basics
import drawable.pygame_drawable 

import pygame
from pygame.locals import *
import unittest

screen = pygame.display.set_mode((1024, 668), DOUBLEBUF)

def init_pygame():
        global screen
        pygame.init()
        screen.fill((0,0,0,255))


WHITE = pygame.Color(255,255,255,255)
RED = pygame.Color(255,0,0,255)

def draw(shape, color = WHITE):
    drawable.pygame_drawable.draw_shape(screen, shape, color)


def collision_color(collision:bool):
    if collision:
        return RED
    else:
        return WHITE

class zz_test(unittest.TestCase):
    screen_cleared = False

    @staticmethod
    def display():
        pygame.display.flip()
        # Il est indispensable de traiter les evenement a un moment. Sinon, Pygame merdois
        pygame.event.pump()

    def clear_screen(self):
        if not self.screen_cleared:
            init_pygame()
        screen_cleared = True


class TestPoint(zz_test):
    screen_cleared = False

    @classmethod
    def init(cls):
        global A, B, C, D, E, F, O
        O = Point(0, 0)
        A = Point(-2, 0)
        B = Point(+2, 0)
        C = Point(0, -2)
        D = Point(0, +2)
        E = Point(-2, +2)
        F = Point(+2, +2)

    def test_eq(self):
        self.init()
        ABis = Point(-2.0, 0.0)
        self.assertTrue(A == ABis)
        self.assertFalse(B == ABis)

        presqueO = Point(0, 0.0000000001)
        self.assertTrue(O, presqueO)

    def test_add(self):
        """ PointAbstract + Vector = PointAbstract"""
        self.init()
        ABis = O + Vector(-2, 0)
        self.assertTrue(A == ABis)
        self.assertFalse(B == ABis)

        # La suite n'est pas mathématiquement propre, mais doit néanmoins marcher
        ABis = O + A
        self.assertTrue(A == ABis)
        self.assertFalse(B == ABis)

        ABis = O  # test inplace add
        ABis += Vector(B)  # test vector.init(tuple)
        self.assertEqual(ABis, B)

    def test_affichage_point(self):
        self.init()
        self.clear_screen()
        point = Point(100,100)
        #point = drawable1.Point (point, (255, 255, 255, 255))
        #point.draw(screen)
        draw( point)
        
        point = Point(200,200)
        #point = drawable1.Point (point, (255, 255, 255, 255))
        #point.draw(screen)
        draw( point)

        point += Vector(4, 4)
        #point = drawable1.Point (point, (255, 255, 255, 255))
        #point.draw(screen)
        draw( point)
        
        self.display()
        print("3 points are drawn")
        time.sleep(2)


class TestSegment(zz_test):
    screen_cleared = False
    @classmethod
    def init(cls):
        global seg1, seg2, seg3
        TestPoint.init()

        seg1=Segment ( A,B )
        seg2=Segment ( (0,-2), (0, 2) )
        seg3=Segment ( (2, 0), (-2,0) ) # that is Segment (B,A)

    def test_eq(self):
        self.init()

        self.assertTrue(seg1 == seg1)
        self.assertTrue(seg1 == seg3)
        self.assertTrue(seg2 != seg3)
        self.assertTrue(seg2 != seg3)


    def test_affichage_Segment(self):
        self.init()
        self.clear_screen()
        seg = Segment((120,100), (120,200))
        #seg = drawable1.Segment (seg, (255, 255, 255, 255))
        #seg.draw(screen)
        draw( seg)

        self.display()
        time.sleep(1)

    def test_collision(self):
        seg1 = Segment( (100,100),(100,200))
        seg2 = Segment( (50,150),(150,150))

        collision = collision_basics.intersection_segment_X_segment_return_boolean(seg1, seg2)

        #seg1 = drawable1.Segment(seg1, collision_color(collision))
        #seg2 = drawable1.Segment(seg2, collision_color(collision))

        self.clear_screen()
        draw( seg1)
        draw( seg2)
        #seg1.draw(screen)
        #seg2.draw(screen)
        self.display()
        time.sleep(1)
        self.assertTrue(collision_basics.intersection_segment_X_segment_return_boolean(seg1, seg2))

        seg3 = Segment((50,50),(50,150))
        collision = collision_basics.intersection_segment_X_segment_return_boolean(seg2, seg3)
        #seg3 = drawable1.Segment(seg3, collision_color(collision))
        #seg3.draw(screen)
        draw( seg3)
        self.display()
        time.sleep(1)

        self.assertTrue(collision_basics.intersection_segment_X_segment_return_boolean(seg2, seg3))
        self.assertFalse(collision_basics.intersection_segment_X_segment_return_boolean(seg1, seg3))

        init_pygame() # clear screen

        #seg1.color = seg2.color=seg3.color=WHITE
        #seg1.draw(screen)
        #seg2.draw(screen)
        #seg3.draw(screen)
        draw(seg1, WHITE)
        draw(seg2, WHITE)
        draw(seg3, WHITE)
        point1 = collision_basics.intersection_segment_X_segment_return_shape(seg1,seg2)
        point2 = collision_basics.intersection_segment_X_segment_return_shape(seg2,seg3)
        draw(point1, RED)
        draw(point2, RED)
        #drawable1.Point(point1, RED).draw(screen)
        #drawable1.Point(point2, RED).draw(screen)
        self.display()
        time.sleep(2)


class TestStraight(zz_test):
    @classmethod
    def init(cls):
        global straight1, straight2, straight3

        straight1 = Straight(O, 0) # ligne horizontale passant par l'origine
        straight2 = Straight(O, math.pi/2) # ligne verticale passant par l'origine
        straight3 = Straight(A, math.pi/4) # droite à 45° passant par (_2,0)

    def test_parametres(self):
        self.init()

        self.assertTrue( straight1.a == 0 )
        self.assertTrue( straight1.b == 0 )

        self.assertAlmostEqual(straight3.a, 1 )
        self.assertAlmostEqual( straight3.b, 2 )

        self.assertEqual( get_x_from_y(straight2, 42), 0 )
        self.assertEqual( get_y_from_x(straight1, 42), 0 )

        self.assertEqual( get_y_from_x(straight3, -2), 0 )

        with self.assertRaises(VerticalLineError):
            straight2.a
        with self.assertRaises(VerticalLineError):
            straight2.b
        with self.assertRaises(HorizontalLineError):
            get_x_from_y( straight1, 42)
        with self.assertRaises(VerticalLineError):
            get_y_from_x( straight2, 42)

class TestAABB_box(zz_test):
    @classmethod
    def init(cls):
        global box1, box2, box3
        TestSegment.init()

        box1 = AABB_box(50, 50, 300, 300)
        box2 = AABB_box(0,-2, 5, 2)
        box3 = AABB_box(50, 300, 300, 50) # Inconsistant as Ymin>Ymax, but I trust the constructor to deal with it

    def test_all(self):
        self.init()

        self.assertEqual(box1, box3)
        self.assertNotEqual(box1, box2)

        gen = box1.points
        first_point = next(gen)
        self.assertEqual(first_point, (300,300) )

        gen = box2.segments
        first_segment = next(gen)
        self.assertEqual(first_segment, Segment((5,2),(0,2)))

class TestPolygon(zz_test):
    screen_cleared = False

    @classmethod
    def init(cls):
        global square, triangle, square2
        square = Polygon(((100, 100), (100, 200), (200, 200), (200, 100)))
        square2 = square.__copy__()
        triangle = Polygon(((150, 150), (300, 300), (1, 300)))

    @unittest.skip("__eq__ is not implemented yet")
    def test_equal(self):
        self.init()
        self.assertEqual(square2, square)
        self.assertNotEqual(square, triangle)
        self.assertEqual(square, square.bounding_box)

    def test_points_and_segments(self):
        self.init()

        self.assertEqual(square.points[0], Point(100,100) )
        self.assertEqual(square.points[0], (100,100) )

        self.assertEqual(triangle.segment(1), Segment((150, 150), (300, 300)) )
        segment_list = list(square.segments)
        self.assertEqual(len(segment_list), 4)
        self.assertEqual(segment_list[0], square.segment(0))

    def test_bounding_box(self):
        self.init()
        self.assertEqual(triangle.bounding_box, AABB_box(1, 150, 300, 300))
        self.assertEqual(square.bounding_box, AABB_box(100, 100, 200, 200))

    def test_affichage_Polygone(self):
        self.init()
        self.clear_screen()
        TestPolygon.init()

        #sqr = drawable1.Polygon(square.points, (255, 255, 255))
        #sqr.draw(screen)
        draw(square)

        self.display()
        time.sleep(1)



if __name__ == '__main__':
    print ("départ")
    unittest.main()
    print ("fini")
    time.sleep(3)