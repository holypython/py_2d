# depracated??
""" implementation using:
 - geometrics based Point(float, float). In py_2D.geometry.*
 - drawable based on pygame. In py_2D.drawable.pygame_drawable"""

from py_2D.drawable.pygame_drawable import *
import py_2D.geometry.primitives as primitives
import geometry.polygon
from geometry.polygon import Polygon as polybase, Point_list

import pygame
from pygame.locals import *

class Point (primitives.Point, PyGamePoint_abstract):

    def __new__(cls, point:primitives.Point, *args):
        """I could use `super` but lets slash overcosts"""
        return tuple.__new__ (cls,(point[0], point[1])) # assuming tuple(float, float)
        # TODO gérer exceptions



    def __init__(self, point:primitives.Point, color:pygame.Color):
        """ point: tuple(float, float)
            color: tuple(int,int,int,int=255)"""

        # __new__ already initialised self as tuple(float, float)
        if isinstance(color, pygame.Color):
            self.color = color
        else:
            try:
                self.color =  pygame.Color(*color) # call Color(r,g,b, a=255)
            except:
                raise TypeError("`color` parameter must be `pygame.Color` or any iterable with 3 or 4 integers")

class Segment(primitives.Segment, PyGameSegment_abstract):

    #def __new__(cls, segment_modele:primitives.Segment, *args):
    #    return tuple.__new__(cls, (segment_modele[0], segment_modele[1]))

    def __new__(cls, segment_modele:primitives.Segment, *args):
        return primitives.Segment.__new__(cls, segment_modele)


    def __init__(self, segment_modele:primitives.Segment, color, line_width=2):
        self.color = color
        self.line_width = line_width

    def get_surface(self):
        width = abs(self.p1.x - self.p2.x)
        height = abs(self.p1.y - self.p2.y)

        result = pygame.Surface((width, height), flags=SRCALPHA)


class Polygon(polygon.Polygon, PyGamePolygon_abstract):
    def __init__(self, point_list:Point_list, color, line_width=2):
        polybase.__init__(self, point_list)
        PyGamePolygon_abstract.__init__(self, color, line_width)

