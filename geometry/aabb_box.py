import itertools
import geometry.primitives

""" Je défini Segment et PointAbstract ainsi pour que les types soient des parametre.
  Si un module externe dit que `aabb_box.PointAbstract = pynum.matrix, alors AABB.Points vas pondre des matrices"""
Segment = geometry.primitives.Segment
Point = geometry.primitives.Point



class AABB_box(geometry.primitives.Geometrics):
    """ an axis-aligned bounding box is a rectangle with one side parallel to the X axis
    and the other to the Y axis. In 2 dimension, it can be fully described with 4 numbers """
    closed = True # This is a closed line

    def __init__(self, x_min, y_min, x_max, y_max):
       # First, we make sure that y_min (resp. y_min) is smaller than x_max (resp. y_max)
        if x_min > x_max:
            x_min, x_max = x_max, x_min  # swap values
        if y_min > y_max:
            y_min, y_max = y_max, y_min  # swap values

        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max  


    @property
    def points(self):
        """ return a generator giving all the points
        usage : for p in my_box.points """
        yield Point(self.x_max,self.y_max )
        yield Point(self.x_min,self.y_max )
        yield Point(self.x_min, self.y_min)
        yield Point(self.x_max, self.y_min)


    @property
    def segments(self):
        """ return a generator giving all the segments
        usage : for s in my_box.segments """
        p1 = Point(self.x_max,self.y_max )
        p2 = Point(self.x_min,self.y_max )
        p3 = Point(self.x_min, self.y_min)
        p4 = Point(self.x_max, self.y_min)

        yield Segment(p1, p2)
        yield Segment(p2, p3)
        yield Segment(p3, p4)
        yield Segment(p4, p1)

    def __repr__(self):
        return "AABB_box({}, {}, {}, {})".format(self.x_min, self.y_min, self.x_max, self.y_max)

    def __eq__(self, other):
        return (self.x_max, self.x_min, self.y_max, self.y_min)\
               == (other.x_max, other.x_min, other.y_max, other.y_min)

def test():
    box1 = AABB_box(100,100, 500, 500)
    box2 = AABB_box(100, 100, 500, 500)
    box3 = AABB_box(200, 200, 300, 300)

    assert box1 == box2
    assert not box1 == box3

if __name__ == "__main__":
    test()




