import math


class OperationTypeError(TypeError):
    def __init__(self, operation_name, operand1, operand2):
        message = "can not perform operation {}.{} ({}, {})"\
            .format(type(self).__name__, operation_name, operand1, operand2)
        TypeError.__init__(self,message)


class AbstractVector:
    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self.x, self.y)

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(other.x+self.x, other.y+self.y)
        elif isinstance(other, Vector):
            return Vector (self.x+other.x, self.y+other.y)
        else:
            try:
                return type(self)(self.x+other[0], self.y+other[1]) # if other is a tuple of float
            except Exception:
                raise OperationTypeError("__add__", self, other)
            
    def __sub__(self, other):
        try:
            return self+(other*(-1))
        except Exception:
                raise OperationTypeError("__sub__", self, other)
            

    def __eq__(self, other: 'AbstractVector'):
        try:
            return (self[0]==other[0] and self[1]==other[1] )
        except Exception:
                raise OperationTypeError("__eq__", self, other)
            
    def __mul__(self, other:float) -> 'AbstractVector':
        return Vector(other*self.x, other*self.y)
    
    def __rmul__(self, other:float) -> 'AbstractVector':
        return Vector(other*self.x, other*self.y)

    def __abs__(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)


class Vector (AbstractVector, tuple):
    def __new__( cls, *args):
        if len(args) == 1:
            return tuple.__new__(cls,args[0]) # assumingVector(tuple)

        try: # assuming Vector(Point, Point) or Vector((float,float),(float,float))
            return tuple.__new__(cls, (args[1][0] - args[0][0], args[1][1] - args[0][1]))
        except (TypeError,IndexError):
            return tuple.__new__(cls, (args[0], args[1]))  # assuming Vector(float, float)


def dot_product(a⃗, b⃗):   # rappel a⃗ = u"a\u20D7"
    return a⃗.x*b⃗.y + a⃗.y*b⃗.x


class Geometrics:
    closed = False # default: this is not a closed line


class AbstractPoint(Geometrics):
    """Almost identical to `Vector`. """
    
    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def points(self):
        yield self

    def __repr__(self):
        return "{}({}, {})".format(type(self).__name__, self[0], self[1])

    def __add__(self, other:AbstractVector) -> 'AbstractPoint':
        """ PointAbstract.add is a translation  """
        try:
            return Point(self[0] + other[0], self[1] + other[1] )
        except Exception:
            raise OperationTypeError("__add__", self, other)

    def __sub__(self, other:AbstractVector) -> 'AbstractPoint':
        try:
            return Point(self[0] - other[0], self[1] - other[1])
        except Exception:
            raise OperationTypeError("__sub__", self, other)

    def __eq__(self, other: 'AbstractPoint'):
        try:
            return (self[0]==other[0] and self[1]==other[1] )
        except Exception:
            raise TypeError ("can not compare type {} and {}".format(type(self).__name__, type(other).__name__))

    def __copy__(self):
        return Point(self.x, self.y)


class Point(AbstractPoint, tuple):

    def __new__(cls, *args):
        try:
            return tuple.__new__ (cls,(args[0], args[1])) # assuming PointAbstract(float, float)
        except Exception:
            return tuple.__new__(cls,(args[0][0], args[0][1])) # assuming PointAbstract( (x,y) )

    def __init__(self, *args):
        pass


class Point_list(list):
    """ this list ensure that each element is (or inherit) an exact type (by default primitives.Point) """
    def __init__(self, list_of_point, exact_type=Point):
        self.exact_type = exact_type
        if isinstance(list_of_point, Point_list):
            list.__init__(self,list_of_point)  # perform a shallow copy. As PointAbstract is immutable, it's like deepcopy
        else:
            list.__init__(self,
                          (p if isinstance(p, exact_type) else exact_type(p) for p in list_of_point) )
        # TODO I should ensure that added point match the `exact_type`

def sorted_by_x(iterable_of_points_or_vector):
    return sorted(iterable_of_points_or_vector, key=lambda point: point.x)

class Segment_list(list):
    closed = False

class Shape_list(list):
    closed = False

class AbstractSegment(Geometrics):
    @property
    def p1(self):
        return self[0]

    @property
    def p2(self):
        return self[1]

    @property
    def points(self):
        yield self.p1
        yield self.p2

    def __eq__(self, other):
        if self[0] == other[0] and self[1] == other[1]:
            return True
        if self[0] == other[1] and self[1] == other[0]:
            return True
        # else
        return False
    
    def __repr__(self):
        return "{} ({}, {})".format(type(self).__name__, self[0], self[1])


class Segment(AbstractSegment, tuple ):
    #def __new__(cls, point1:AbstractPoint, point2:AbstractPoint):
    def __new__(cls, *args):
        if len(args) == 2:
            return tuple.__new__(cls, (args[0], args[1]))
        elif len(args) == 1:
            return tuple.__new__(cls, (args[0][0], args[0][1]))

    #def __init__(cls, point1: AbstractPoint, point2: AbstractPoint):
    def __init__(cls, *args):
        # TODO : documenter et gérer les exceptions
        pass



class StraightError (Exception):
    pass


class HorizontalLineError (StraightError):
    pass


class VerticalLineError (StraightError):
    pass


class CollinearLineError (StraightError):
    pass


class Straight(): # do not inherit geometrics as there is not a `points->iterable`
    """ une droite formé par 1 point et un angle en radiant
    It is very possible that I trash that and define it with 2 points"""
    closed = False

    def __init__(self, point:AbstractPoint, angle:float):
        super().__init__()
        self.point = point
        self.angle = angle % math.pi

    @property
    def a(self):
        """Give the parameter `a` in the equation `y=ax+b` """
        if math.isclose(self.angle, math.pi):
            return 0
        if self.angle == math.pi/2:
            raise VerticalLineError('cannot decide "a" for a vertical line')
        return math.tan(self.angle)  # TODO: a vérifier
                                    # TODO: prevoir précalcul

    @property
    def b(self):
        """Give the parameter `b` in the equation `y=ax+b` """
        try:
            return self.point.y - self.point.x * self.a
        except VerticalLineError:
            raise VerticalLineError('cannot decide "b" for a vertical line')
        
    def __str__(self):
        try:
            return "Straight y = {} * x + {}".format(self.a, self.b)
        except VerticalLineError:
            return "Vertical Straight passing throught "+ str(self.point)

def get_y_from_x(straight:Straight, x:float):
        try:
            return straight.a*x + straight.b
        except VerticalLineError:
            raise VerticalLineError("can't get y from x on a vertical line")

def get_x_from_y(straight:Straight, y:float):
        try:
            return (y-straight.b)/straight.a
        except ZeroDivisionError:
            raise HorizontalLineError("can't get x from y on a horizontal line")
        except VerticalLineError:
            return straight.point.x

# TODO: relire ce morceau à jeun
def get_point_from_straight(straight, x=None, y=None):
        """ return the point where x=param.x or y=param.y
        usge: getpoint(x=4) """
        if (x, y) == (None, None):
            raise ValueError ("function get_point_from_straight need either `x` or `y` as parameter (both where None) ")

        if y is None:  # we try to get y from x
            y = straight.a * x + straight.b  # may raise VerticalLineError.

        if x is None:
            x = straight.get_x_from_y(y)  # may raise HorizontalLineError

        return Point(x, y)


if __name__ == "__main__":
    p1 = Point(2, 3)
    assert isinstance(p1, Point)
    assert p1 == (2, 3)

    p2 = Point(p1.x, p1.y)
    assert p1 == p2
    p1 = Point(p2)
    assert p1 == p2
    assert not p1 is p2

    v = Vector(1, 1)
    assert isinstance (v, Vector)
    p3 = p1 + v

    assert p1 != p3
    assert p3 == (3, 4)
    assert isinstance(p3, Point)

    v2 = Vector(v)

    assert v2 == v
    assert v2 is not v
    assert isinstance(v2, Vector)
    assert v + v2 == (2, 2)
    assert type(v + v2) == Vector
    assert v + v2 == 2 * v == 2 * v2
    assert 2 * v == v * 2
    assert type(2 * v) == type(v * 2) == Vector

    p1, p2 = Point(-1,0), Point(1,0)
    p3, p4 = Point(0,-1), Point(0,1)
    
    
    s1 = Segment(p1, p2)
    s2 = Segment(p3, p4)

    O = Point(0,0)

    st1 = Straight(O,0) # ligne horizontal passant par l'origine
    st2 = Straight(O, math.pi/2) # ligne vertical passant par l'origine
    st3 = Straight(p1, 0.01)
    print(st1, "\n",st2, "\n",st3)

    list1 = Point_list (( (1,1),[2,2], p1 ))
    list2 = Point_list(list1)

    assert list1 == list2
