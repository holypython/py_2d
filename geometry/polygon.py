from .aabb_box import AABB_box
from .primitives import *
import math

""" Everything in this library still is abstract as, at this point, nothing is decided about
the concrete implementation of the geometry (standard tuple? Pynum? other?) """

class Polygon(Geometrics):
    """  This represent a polygon in 2D
    internal structure is a list of points"""

    closed = True # this is a closed line

    def __init__(self, point_list:Point_list):
        """ a polygon can be created from a list of any points
         a list of point is a sequence with 2 level of depth: ( (2,4), (3,5), (10,50) )"""
        # self.points : Point_list

        if isinstance(point_list, Point_list):
            self.points = Point_list(point_list)
        else:
            self.points = Point_list( [Point(p[0], p[1]) for p in point_list] )

    def __copy__(self):
        return Polygon(self.points)

    def segment(self, index):
        """ return a given segment"""
        return Segment(self.points[index], self.points[index - 1])

    @property
    def segments(self):
        """return iterator providing each segment according from self.points
        The first yield is Segment(points[-1],points[0]) and the last is Segment(points[-2],points[-1])"""
        return (Segment (p1, p2) for p1, p2 in zip(self.points, self.points[-1:]+self.points[:-1] ))

    @property
    def bounding_box(self)->AABB_box:
        """ return a AABB_box including the polygon"""

        first_vertex = self.points[0]
        x_min = first_vertex.x
        x_max = first_vertex.x
        y_min = first_vertex.y
        y_max = first_vertex.y

        for vertex in self.points:
            if vertex.x < x_min:
                x_min = vertex.x
            else:
                if vertex.x > x_max:
                    x_max = vertex.x

            if vertex.y < y_min:
                y_min = vertex.y
            else:
                if vertex.y > y_max:
                    y_max = vertex.y

        return AABB_box(x_min, y_min, x_max, y_max)

def test():

    p1 = Polygon([(0, 0), (0, 10), (10, 10), (20, 20), (20, 0)])

    for s in p1.segments:
        print(s)

    assert p1.bounding_box == AABB_box(0,0,20,20)

    p2 = Polygon( ( (5,5),(5,30),(30,30),(30,5) ) )

    for s in p2.segments:
        print(s)

    assert p2.bounding_box == AABB_box(5, 5, 30, 30)

    p3 = Polygon([(0, 0), (0, 10), (10, 10), (20, 20), (20, 0), (0, 10), (10, 10), (20, 20), (20, 0), (0, 10), (10, 10), (20, 20), (20, 0)])

    from zz_chrono import display_all, getChrono

    for i in range(10000):
        with getChrono("segments"):
            dummy = [s for s in p3.segments]

    display_all()


if __name__ == "__main__":
    test()